[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=mikel-m_pruebas&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=mikel-m_pruebas)
![Codiga Quality Score](https://api.codiga.io/project/36156/score/svg)
# Prueba test
Este proyecto es una aplicación de impuestos que se está probando. El archivo gitlab-ci.yml incluido en este proyecto contiene diferentes etapas para compilar, probar y desplegar la aplicación.

## Configuración
### Imagen base
La imagen base utilizada para compilar y probar la aplicación es jangrewe/gitlab-ci-android.

### Variables
Las variables definidas en el archivo son las siguientes:
- ANDROID_HOME: ruta donde está instalado el SDK de Android.
- EMULATOR: ruta al emulador de Android.
- SNYK_TOKEN: token de Snyk para escaneo de seguridad.
- OSTORLAB_API_KEY: clave de API de Ostorlab para escaneo de seguridad.
- SONAR_USER_HOME: ubicación de la caché de la tarea de análisis de SonarCloud.
- GIT_DEPTH: define la profundidad de clonado del repositorio.

### Etapas
#### Etapa 1: Compilar
La primera etapa, llamada assembleDebug, compila la aplicación y genera un archivo .apk en la ruta app/build/outputs/apk/debug/app-debug.apk.

#### Etapa 2: Pruebas Unitarias
La segunda etapa, llamada test, ejecuta las pruebas unitarias de la aplicación utilizando el comando ./gradlew test.

#### Etapa 3: Análisis de seguridad con MobSF
La tercera etapa, llamada mobsfscan, ejecuta una prueba de seguridad utilizando MobSF. Esta prueba se ejecuta en una imagen de Docker de Python, y se instala el paquete MobSF antes de ejecutar el escaneo.

#### Etapa 4: Análisis de seguridad con Ostorlab
La cuarta etapa, llamada runScanOstorlab, utiliza una imagen de Docker de Ostorlab para realizar un análisis de seguridad en la aplicación generada. Se especifica la ruta del archivo .apk generado en la etapa de compilación.

#### Etapa 5: Análisis de seguridad con SonarCloud
La quinta y última etapa, llamada sonarcloud-check, realiza un análisis de calidad utilizando SonarCloud. Se utiliza la tarea sonarqube incluida en el archivo build.gradle para ejecutar el análisis. El informe de análisis se almacena en la carpeta .sonar/cache.

#### Etapa 6: Upload apk a Google Drive
La sexta etapa, llamada upload_to_drive, sube el apk generado en la etapa build a Google Drive y genera un link para visualizar el archivo y otro link para descargar directamente el apk. Utiliza el script "app.js" que tiene tres funciones: uploadFile, updateFile y generatePublicUrl. 
- uploadFile: sube el archivo apk a la carpeta de Google Drive. Si ya hay un archivo apk, genera otro (no lo sobrescribe).
- updateFile: actualiza el archivo apk de Google Drive (el id sigue siendo el mismo).
- generatePublicUrl: genera dos links:
  - webViewlink: este link se genra para poder visualizar el archivo apk (acceso al archivo).
  - webContentLink: este es un link para descargar directamente el archivo apk.

Fuente: [How to use Google Drive API to upload, delete and create a public URL for a file.](https://www.youtube.com/watch?v=1y0-IfRW114&ab_channel=MafiaCodes) 

## Herramientas de análisis de código (Gitlab)

| Herramienta | Lenguajes soportados                          | Link                                                                                                 | Notas                                                                                                        | Gratis   | OWASP             |
|-------------|-----------------------------------------------|------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------|----------|-------------------|
| mobsfscan   | Java, Kotlin, Swift, Objective C Code         | [https://github.com/MobSF/mobsfscan](https://github.com/MobSF/mobsfscan)                             | Visualizar desde el pipeline. Análisis rápido. No sigue OWASP pero incluye pruebas recomendadas por OWASP.   | Sí       | No (directamente) |
| Ostorlab    | Android e iOS                                 | [https://www.ostorlab.co/](https://www.ostorlab.co/)                                                 | CI/CD y análisis completo de pago.                                                                           | Limitado | Sí                |
| owasp-zap   | HTML, JavaScript, CSS, PHP, Java, Python…     | [https://www.zaproxy.org/](https://www.zaproxy.org/)                                                 | Por probar.                                                                                                  | Sí       | Sí                |
| SonarCloud  | Java, php, python, C, C++, C\#, html, Kotlin… | [https://www.sonarsource.com/products/sonarcloud/](https://www.sonarsource.com/products/sonarcloud/) | Visualización desde la web. Bastante completo. Repositorio privado de pago. (Puede ser la mejor opción).     | Limitado | Sí                |
| AndroBugs   | Android (Java y Kotlin), C/C++                | [https://github.com/AndroBugs/AndroBugs_Framework](https://github.com/AndroBugs/AndroBugs_Framework) | No muy completo. Análisis muy rápido. No actualizado.                                                        | Sí       | No                |
| Codiga      | C, JavaScript, Python, php, Java, Kotlin...   | [https://www.codiga.io/](https://www.codiga.io/)                                                     | Analisis desde CI/CD visualizar en web. Hay opciones mejores (puede ser más completo).                       | Limitado | No (directamente) |
| QARK        | APK                                           | [https://github.com/linkedin/qark](https://github.com/linkedin/qark)                                 | Por probar.                                                                                                  | Sí       | Sí                |
| Codacy      | JavaScript, php, Java, SQL...                 | [https://www.codacy.com/](https://www.codacy.com/)                                                   | Herramienta estándar, cumple pero no destaca.                                                                | Limitado | No (directamente) |
| SonarQube   | Java, php, python, C, C++, C\#, html, Kotlin… | [https://www.sonarsource.com/products/sonarqube/](https://www.sonarsource.com/products/sonarqube/)   | Por probar pero igual que SonarCloud. [Documentación.](https://docs.sonarqube.org/latest/try-out-sonarqube/) | Limitado | Sí                | 

## Herramientas de análisis de código (Github)

| Herramienta | Lenguajes soportados                            | Link                                                                                                 | Notas                                                                                                        | Gratis   | OWASP             |
|-------------|-------------------------------------------------|------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------|----------|-------------------|
| SonarCloud  | Java, php, python, C, C++, C\#, html, Kotlin…   | [https://www.sonarsource.com/products/sonarcloud/](https://www.sonarsource.com/products/sonarcloud/) | Visualización desde la web. Bastante completo. Repositorio privado de pago. (Puede ser la mejor opción).     | Limitado | Sí                |
| Codiga      | C, JavaScript, Python, php, Java, Kotlin...     | [https://www.codiga.io/](https://www.codiga.io/)                                                     | Analisis desde CI/CD visualizar en web. Hay opciones mejores (puede ser más completo).                       | Limitado | No (directamente) |
| QARK        | APK                                             | [https://github.com/linkedin/qark](https://github.com/linkedin/qark)                                 | Por probar.                                                                                                  | Sí       | Sí                |
| CodeClimate | Java, JavaScript, php, Python...                | [https://codeclimate.com/](https://codeclimate.com/)                                                 | Por probar.                                                                                                  | Sí       | No (directamente) |
| Codacy      | JavaScript, php, Java, SQL...                   | [https://www.codacy.com/](https://www.codacy.com/)                                                   | Herramienta estándar, cumple pero no destaca.                                                                | Limitado | No (directamente) |
| Kiuwan      | C, Java, JavaScript, php, Python...             | [https://www.kiuwan.com/](https://www.kiuwan.com/)                                                   | Por probar.                                                                                                  | No       | Sí                |
| SonarQube   | Java, php, python, C, C++, C\#, html, Kotlin…   | [https://www.sonarsource.com/products/sonarqube/](https://www.sonarsource.com/products/sonarqube/)   | Por probar pero igual que SonarCloud. [Documentación.](https://docs.sonarqube.org/latest/try-out-sonarqube/) | Limitado | Sí                | 


Enlace a la [documentación de las herramientas](https://docs.google.com/document/d/1Pg_M4uiLzuCFAZfog_foaixQlmhtcvxxhvfCSQ2Pd8k/edit?usp=sharing).