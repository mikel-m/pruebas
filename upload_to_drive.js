const { google } = require('googleapis');
const path = require('path')
const fs = require('fs')

//const CLIENT_ID = '777319225230-1f3uvh217d4qhkc8qh46i60odd9nfdos.apps.googleusercontent.com'
const CLIENT_ID = '765637600336-p3c3d27ef5vo7bm0p2bhemahj3gb4ls7.apps.googleusercontent.com'
//const CLIENT_SECRET = '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'
const CLIENT_SECRET = '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'
const REDIRECT_URI = 'https://developers.google.com/oauthplayground'

const REFRESH_TOKEN = '1//04_cQkWgUDjggCgYIARAAGAQSNwF-L9IrKestH4VvSb4ckAa6bEihz932oDlzoqu9RW2nyb09EHe-0qO8dvP3hKQmX_3j8DfeAF4'

const oauth2Client = new google.auth.OAuth2(
    CLIENT_ID,
    CLIENT_SECRET,
    REDIRECT_URI
);

oauth2Client.setCredentials({refresh_token: REFRESH_TOKEN})

const drive = google.drive({
    version: 'v3',
    auth: oauth2Client
})

// Lee el archivo build.gradle y extrae el versionName
const buildGradleFile = fs.readFileSync('app/build.gradle', 'utf8');
const versionName = buildGradleFile.match(/versionName\s+"([^"]+)"/)[1];

// Para saber la version
const argv = require('minimist')(process.argv.slice(2));
const version = argv['version'] || process.env.VERSION_NAME || 'unknown';

//const filePath = path.join(__dirname, "app/build/outputs/apk/debug/app-debug.apk")
const filePath = path.join(__dirname, `app/build/outputs/apk/debug/app-debug-${versionName}.apk`);


/*
async function uploadFile() {
    try {
        const folderID = '1yDnKKIusBoInJkZLLaChZEWtBOozmAUk'
        const response = await drive.files.create({
            requestBody: {
                name: 'app-debug-${version}.apk',
                mimeType: 'application/vnd.android.package-archive',
                parents: [folderID]
            },
            media: {
                mimeType: 'application/vnd.android.package-archive',
                body: fs.createReadStream(filePath)
            }
        })
        console.log(response.data)
    } catch (error) {
        console.log(error.message)
    }
}
*/
async function uploadFile() {
  try {
    const folderID = '1yDnKKIusBoInJkZLLaChZEWtBOozmAUk';
    const versionName = await getVersionName(); // Obtener versión del build.gradle
    const filename = `app-debug-${versionName}.apk`;
    const response = await drive.files.create({
      requestBody: {
        name: filename,
        mimeType: 'application/vnd.android.package-archive',
        parents: [folderID]
      },
      media: {
        mimeType: 'application/vnd.android.package-archive',
        body: fs.createReadStream(filePath)
      }
    });
    console.log(response.data);
  } catch (error) {
    console.log(error.message);
  }
}

/*
async function updateFile(fileId) {
    try {
        const response = await drive.files.update({
            fileId,
            media: {
                mimeType: 'application/vnd.android.package-archive',
                body: fs.createReadStream(filePath)
            }
        })
        console.log(response.data)
    } catch (error) {
        console.log(error.message)
    }
}
*/
function getVersionName() {
  const buildGradle = fs.readFileSync('./app/build.gradle', 'utf8');
  //const regex = /versionName "([\d.]+)"/;
  const regex = /versionName "([0-9.]+)"/;
  const match = buildGradle.match(regex);
  return match[1];
}
async function updateFile(fileId) {
  try {
    const versionName = await getVersionName(); // Obtener versión del build.gradle
    const filename = `app-debug-${versionName}.apk`;
    const response = await drive.files.update({
      fileId,
      media: {
        mimeType: 'application/vnd.android.package-archive',
        body: fs.createReadStream(filePath)
      },
      requestBody: {
        name: filename
      }
    });
    console.log(response.data);
  } catch (error) {
    console.log(error.message);
  }
}

async function deleteFile() {
    try {
        const response = await drive.files.delete({
            fileId: '1BmRk_KAQH-BlzgnDuWcjSdEUhf0AcXix'
        });
        console.log(response.data, response.status)
    } catch (error) {
        console.log(error.message)
    }
}

async function generatePublicUrl(fileId) {
    try {
        await drive.permissions.create({
            fileId: fileId,
            requestBody: {
                role: 'reader',
                type: 'anyone'
            }
        })
        const result = await drive.files.get({
            fileId: fileId,
            fields: 'webViewLink, webContentLink'
        })
        console.log(result.data)
    } catch (error) {
        console.log(error.message)
    }
}

/*
async function getFileVersion(fileId){
    try {
        const response = await drive.files.get({
           fileId: fileId,
           fields: 'version'
        });
        const version = response.data.version;
        console.log('La version del APK es: ', version);
    } catch (error) {
        console.log(error.message);
    }
}
*/
async function getFileVersion(fileId){
    try {
        const response = await drive.files.get({
           fileId: fileId,
           fields: 'name'
        });
        const name = response.data.name;
        const regex = /app-debug-(\d+\.\d+)\.apk/;
        const match = regex.exec(name);
        const version = match[1];
        console.log('La version del APK es: ', version);
    } catch (error) {
        console.log(error.message);
    }
}


//deleteFile();
//uploadFile();
updateFile('1rWB3EULdXDKk8mSZp0l-bh5gqEqBzzgO');
generatePublicUrl('1rWB3EULdXDKk8mSZp0l-bh5gqEqBzzgO');
getFileVersion('1rWB3EULdXDKk8mSZp0l-bh5gqEqBzzgO');