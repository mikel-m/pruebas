#!/usr/bin/env bash

firebase appdistribution:distribute app/build/outputs/apk/debug/app-debug.apk   \
    --token "$FIREBASE_TOKEN" \
    --app "$APP_ID" \
    --release-notes "Version de prueba" --testers-file testers.txt